import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/scss/main.scss'
import axios_http from './plugins/axios'

Vue.config.productionTip = false

Vue.prototype.$axios = axios_http

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
