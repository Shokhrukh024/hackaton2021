import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter) 

const routes = [
  {
    path: '/',
    component: () => import('../layouts/Empty.vue'),
    children: [{ path: '', name: 'Home', component: () => import('../views/Login.vue'), meta: {authRequired: true}}]
  },
  {
    path: '/home',
    component: () => import('../layouts/MainLayout.vue'),
    children: [
      {path: '/', name: 'Home', component: () => import('../views/Home.vue'), meta: {authRequired: true}},
    ]
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})





// router.beforeEach(async (to, from, next) => {
//   let user = await store.getters.getUser;
//   if (to.matched.some(route => route.meta.authRequired)) {
//     if(user.token != ''){
//       next()
//     } else{
//       if(sessionStorage.getItem('user')){
//         const data = JSON.parse( sessionStorage.getItem('user') );
//         await store.dispatch('STATE_CHANGED', data);
//         next()
//       }else{
//         next('/')
//       }
      
//     }
//   }
//   else{
//     next()
//   }
// })





export default router
