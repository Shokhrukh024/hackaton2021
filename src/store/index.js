import Vue from 'vue'
import Vuex from 'vuex'
import UserModule from './user'
import CardsModule from './cards'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    UserModule,
    CardsModule
  }
})
