import axios from "axios";

export default{
    state:{
        profiles_list: [],
        categories_list: []
    },
    mutations:{
        SET_PROFILES_LIST: (state, payload) => {
            state.profiles_list = payload
        },
        SET_CATEGORIES_LIST: (state, payload) => {
            state.categories_list = payload
        },
    },
    actions: {
        async GET_PROFILES_LIST({commit}, payload) {
          return await axios({
              method: "GET",
              url: 'https://0920-84-54-80-150.ngrok.io/brandigram/instagram/profile/list/?category_id=' + payload,
            })
            .then((e) => {
              commit('SET_PROFILES_LIST', e.data);
            })
            .catch((error) => {
              return error;
            })
        },


        async GET_CATEGORIES_LIST({commit}, payload) {
            return await axios({
                method: "GET",
                url: 'https://0920-84-54-80-150.ngrok.io/brandigram/instagram/category/list/ ',
              })
              .then((e) => {
                commit('SET_CATEGORIES_LIST', e.data);
              })
              .catch((error) => {
                return error;
              })
          },
    },
    getters:{
        getProfilesList: state => state.profiles_list,
        getCategoriesList: state => state.categories_list,

    }
}
